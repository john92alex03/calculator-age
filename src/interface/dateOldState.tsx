
export interface dateState {
  day :number,
  month :number,
  year :number,
  ageOld :number,
  monthOld :number | null,
  dayOld :number,
  hoursOld :number,
  minutesOld :number,
  secondsOld :number,
  millisecondsOld :number
}