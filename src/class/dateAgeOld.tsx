

export class DateAgeOld {
  private ageOld :number;
  private year :number;
  private month :number;
  private day :number;
  private monthOld :number = 0;
  private dayOld :number = 0;
  private hoursOld :number = 0;
  private minutesOld :number = 0;
  private secondsOld :number = 0;
  private millisecondsOld :number = 0;
  private dateActually :Date = new Date()
  
  
  constructor(ageOld :number, year :number, month: number, day :number ) {
    this.ageOld = ageOld;
    this.year = year;
    this.month = month;
    this.day = day;
  }

  
  private getDayMonth () :number {

    let totalsDayMonthLast :number = new Date(this.year, this.dateActually.getMonth(), 0).getDate()
    let dayActually :number = this.dateActually.getDate()
    
    let daysPassedMonth :number = dayActually - this.day
 
    if(this.day >= dayActually) {

      let resultDay :number = this.day - dayActually 
      daysPassedMonth = totalsDayMonthLast - resultDay
      
      this.monthOld = this.monthOld - 1

    } else if(this.day < dayActually){
     
      let daysPassed :number = totalsDayMonthLast - this.day
      daysPassedMonth = totalsDayMonthLast - daysPassed
      
      this.monthOld = this.monthOld

    } 

    return daysPassedMonth

  }

  get getMonthOld() {
    
    let monthActually :number = this.dateActually.getMonth()
    let dayActually :number = this.dateActually.getDate()
    
    if( this.month < monthActually + 1 ){
      let resultMonth :number =  (monthActually + 1) - this.month
      this.monthOld = resultMonth 
      this.dayOld = dayActually
    } 
    else {
      
      let resultMonth :number = this.month - (monthActually + 1)
      if (resultMonth == 0) {
        this.monthOld = 12 
      } else {
        let result = this.month - (monthActually + 1)
        this.ageOld = this.ageOld + 1
        this.monthOld = 12 - result 
      }      

    }

    this.dayOld = this.getDayMonth()
    return this.monthOld
  }
  
  get getDayOld() {
    
    return this.dayOld
  }

  get getHoursOld(){
    this.hoursOld = this.dateActually.getHours()
    return this.hoursOld
  }

  get getMinutesOld() {
    this.minutesOld = this.dateActually.getMinutes()
    return this.minutesOld
  }

  get getSecondsOld(){
    this.secondsOld = this.dateActually.getSeconds()
    return this.secondsOld
  }

  get getMillisecondsOld(){
    this.millisecondsOld = this.dateActually.getMilliseconds()
    return this.millisecondsOld
  }

}