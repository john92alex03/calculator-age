'use client'

import { useState } from "react"
import { DateAgeOld } from "../../class/dateAgeOld"
import { dateState } from '../../interface/dateOldState'

export default function AgeCalculator () {
  const [dateResult , setDateResult ] = useState<dateState>(initialState)

  const [ intervalTimeDate, setIntervalTimeDate  ] = useState(0)

  const dateActually :Date = new Date()
  let arrayDate :number[] = []
  let year :number = 0
  let month :number = 0
  let day :number = 0
  let ageOld :number = 0

  function calculateAge(event ?:any ) : void {
  
    event.preventDefault()
    const dateInput :string = event.target[0].value
    let intervalTime :NodeJS.Timer 
    
    for(let i = 0; i < dateInput.length; i++){
      arrayDate.push(Number(dateInput[i]))
    }
    
    year = Number(arrayDate.slice(0,4).join(''))
    month = Number(arrayDate.slice(5,7).join(''))
    day = Number(arrayDate.slice(8,10).join(''))
    
    if( month > ( dateActually.getMonth() + 1 )){
      ageOld = (dateActually.getFullYear() - year) - 1
    } else if( month == (dateActually.getMonth() + 1) && day > dateActually.getDate() ) {
      ageOld = dateActually.getFullYear() - year - 1
    } else if( month == (dateActually.getMonth() + 1) && day <= dateActually.getDate() ) {
      ageOld = dateActually.getFullYear() - year 
    }
     else {
      ageOld = (dateActually.getFullYear() - year)
    }

    
      intervalTime = setInterval(()=> {
       
        const ageOldResult = new DateAgeOld(ageOld,year,month,day,)
          setDateResult({
            day ,
            month ,
            year,
            ageOld ,
            monthOld : ageOldResult.getMonthOld,
            dayOld : ageOldResult.getDayOld,
            hoursOld : ageOldResult.getHoursOld,
            minutesOld : ageOldResult.getMinutesOld,
            secondsOld : ageOldResult.getSecondsOld,
            millisecondsOld : ageOldResult.getMillisecondsOld
          })
  
      }, 1000)
    
    

    setIntervalTimeDate(Number(intervalTime))

  }

  
  function resetDate() :void  {
    clearInterval(intervalTimeDate)
    setDateResult(initialState)
  }

  return (
    <>
    <div className="z-40 mt-6 w-full font-bold text-center items-center  justify-center flex cursor-pointer">
      <form action='' className="w-auto flex h-auto" name='date' id='date' method="POST" onSubmit={(e)=> calculateAge(e) } >
        <input type="date" placeholder="Select date" className="text-center  text-lg mr-3 mt-0.5 h-10 p-1 pl-2 rounded-md text-slate-800" />
        <button className="w-auto shadow-md border text-xl font-serif translate-x-1 transition duration-150 ease-in-out rounded-2xl p-2 hover:bg-orange-400 border-gray-600 hover:border-orange-400">
          calcula tu edad
        </button>
      </form>
        <button className="ml-5 w-3/12 border shadow-md text-xl font-serif transition duration-150 ease-in-out rounded-2xl p-2 hover:bg-red-400 border-gray-600 hover:border-red-400" onClick={()=> resetDate()}>
          reset
        </button>
    </div>
      { dateResult.monthOld === null ? '' : 
        <div className="pt-6 text-xl text-center font-bold">
          tienes {dateResult.ageOld} años - {dateResult.monthOld} meses - {dateResult.dayOld} dias - {dateResult.hoursOld} horas - {dateResult.minutesOld} minutos - <br />
          {dateResult.secondsOld} segundos
        </div> 
        }
    </>
  )
}

const initialState = {
   day : 0,
    month : 0,
    year : 0,
    ageOld : 0,
    monthOld : null,
    dayOld : 0,
    hoursOld : 0,
    minutesOld : 0,
    secondsOld : 0,
    millisecondsOld: 0
}

