import './globals.css'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Age calculator',
  description: 'App for calculator the age',
}

export default function RootLayout({children,} :{ children: React.ReactNode } ) {
  return (
    <html lang="en" className='min-h-screen min-w-full h-auto'>
      <body className={inter.className}>{children}</body>
    </html>
  )
}
