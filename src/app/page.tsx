import Image from 'next/image'
import AgeCalculator from './components/AgeCalculator'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-center z-10 min-w-screen w-full bg-gradient-to-br from-green-200 to-amber-300">
      <div className='border-double bg-gradient-to-r from-orange-200 to-orange-200 rounded-xl p-24 shadow bg-opacity-20 z-30 drop-shadow-xl'>
        <h1 className='z-30 text-center text-3xl font-bold tracking-light bg-gradient-to-r from-gray-900 to-slate-600 bg-clip-text text-transparent'> Quieres saber cuanto tiempo llevas viviendo </h1>
        <AgeCalculator />
      </div>
    </main>
  )
}
